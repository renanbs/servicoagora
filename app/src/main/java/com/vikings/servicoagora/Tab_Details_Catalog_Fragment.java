package com.vikings.servicoagora;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Tab_Details_Catalog_Fragment extends Fragment {

	private RecyclerView mRecyclerView;
	private Service_List_Adapter mServiceListAdapter;

	private RecyclerView.LayoutManager mLayoutManager;

	private ArrayList<Service_List_Model> mServiceList;
	String mServerUrl;
	String mService;
	int mPosition;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Get back arguments
		mPosition = getArguments().getInt("position", 0);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.tab_details_catalog_fragment, container, false);

		// Services List recycler
		mRecyclerView = (RecyclerView) v.findViewById(R.id.catalog_recyclerview);
		mLayoutManager = new LinearLayoutManager(getContext());
		mRecyclerView.setLayoutManager(mLayoutManager);

		SharedPreferences settings = getActivity().getSharedPreferences(getString(R.string.settings_preferences), getActivity().MODE_PRIVATE);
		mServerUrl = settings.getString(getString(R.string.settings_server_url), getString(R.string.settings_server_url_default));
		mService = settings.getString(getString(R.string.settings_services_list), getString(R.string.settings_server_url_default));

		if (mServiceList == null)
			mServiceList = new ArrayList<>();

		getServices(mPosition);

		mServiceListAdapter = new Service_List_Adapter(mServiceList);
		mServiceListAdapter.setOnItemClickListener(new Service_List_Adapter.OnServiceItemClickListener() {
			@Override
			public void onItemClick(View view, int position) {
//				int id = mEcList.get(position).getId();
				Intent intent = new Intent(getActivity(), ContractActivity.class);
//				intent.putExtra("position", id);
				startActivity(intent);
			}
		});

		mRecyclerView.setAdapter(mServiceListAdapter);

		return v;
	}

	private void getServices(int id)
	{
		String url = mServerUrl + mService + "?id=" + id;
//		mProgressDialog.setMessage("Loading...");
//		mProgressDialog.show();

		final JsonArrayRequest req = new JsonArrayRequest(url,
				new Response.Listener<JSONArray>() {
					@Override
					public void onResponse(JSONArray response) {
						Log.e("Size = " + response.length() + " : ", response.toString());
						try {
							if(response.length() > 1)
								throw new ArrayIndexOutOfBoundsException("Wrong response size...");
						}
						catch (ArrayIndexOutOfBoundsException e) {
							Log.e("Error: Out of bounds", e.getMessage());
						}
						try{
							JSONObject c = response.getJSONObject(0);
							int id = c.getInt("id");
							// Lista de serviços (Corte de cabelo, unha)
							JSONArray list = c.getJSONArray("details");
							for (int a = 0; a < list.length(); a++) {
								JSONObject o = list.getJSONObject(a);
								int serviceId = o.getInt("id");
								String serviceName = o.getString("name");
								double minValue = o.getDouble("minval");
								double maxValue = o.getDouble("maxval");
								//Lista de trabalhadores
								JSONArray workersJson = o.getJSONArray("workers");
								ArrayList<String> workers = new ArrayList<>();
								for (int w = 0; w < workersJson.length(); w++) {
									workers.add(workersJson.getString(w));
								}
								Service_List_Model serviceList = new Service_List_Model(serviceId, serviceName, minValue, maxValue, workers);
								mServiceList.add(serviceList);
							}
						}
						catch(JSONException e)
						{
							Log.e("Error: Exception", e.getMessage());
						}

						mServiceListAdapter.notifyDataSetChanged();
//						mProgressDialog.hide();
					}
				}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				VolleyLog.d("Error", "Error: " + error.getMessage());
//				mProgressDialog.hide();
			}
		});

		// Adding request to request queue
		ServerAccess.getInstance(getContext()).addToRequestQueue (req);
	}
}
package com.vikings.servicoagora;

import java.util.ArrayList;

/**
 * Created by renan on 5/13/16.
 */
public class Service_List_Model {
	private int mId;
	private String mServiceName;
	private double mPriceMin;
	private double mPriceMax;
	private ArrayList<String> mWorkersList;

	public Service_List_Model(int id, String serviceName, double priceMin, double priceMax, ArrayList<String> workersList){
		mId = id;
		mServiceName = serviceName;
		mPriceMin = priceMin;
		mPriceMax = priceMax;
		mWorkersList = workersList;
	}

	public int getId() {
		return mId;
	}
	public String getServiceName() {
		return mServiceName;
	}
	public double getPriceMin() { return mPriceMin; }
	public double getPriceMax() { return mPriceMax; }
	public ArrayList<String> getWorkersList() {
		return mWorkersList;
	}

	public void setWorkersList(ArrayList<String> workers){
		mWorkersList = workers;
	}

}

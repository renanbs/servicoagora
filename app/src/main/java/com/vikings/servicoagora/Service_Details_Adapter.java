package com.vikings.servicoagora;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;


/**
 * Created by renan on 05/13/16.
 */
public class Service_Details_Adapter extends
		RecyclerView.Adapter<Service_Details_Adapter.ViewHolder>{

	private ArrayList<Service_Details_Model> mServiceDetails;
	private static RecyclerView mCreditCardsRecycler;

	public Service_Details_Adapter(ArrayList<Service_Details_Model> serviceDetails) {
		mServiceDetails = serviceDetails;
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {
		public ViewHolder(View v) {
			super(v);
		}
	}

	public class Services_ViewHolder extends ViewHolder {
		TextView mServiceName, mLargeDescription, mAddress, mCategory, mRate, mDistance, mBusinessHours;

		private Credit_Card_List_Adapter mCreditCardsAdapter;
		public Services_ViewHolder(View v) {
			super(v);
			Context context = itemView.getContext();
			this.mServiceName = (TextView) v.findViewById(R.id.info_name);
			this.mLargeDescription = (TextView) v.findViewById(R.id.info_large_description);
			this.mAddress = (TextView) v.findViewById(R.id.info_address);
			this.mCategory = (TextView) v.findViewById(R.id.info_category);
			this.mRate = (TextView) v.findViewById(R.id.info_rating);
			this.mDistance = (TextView) v.findViewById(R.id.info_distance);
			this.mBusinessHours = (TextView) v.findViewById(R.id.info_open_time);

			mCreditCardsRecycler = (RecyclerView) v.findViewById(R.id.workers_list);
			mCreditCardsAdapter = new Credit_Card_List_Adapter();
			mCreditCardsRecycler.setAdapter(mCreditCardsAdapter);
			mCreditCardsRecycler.setLayoutManager(new LinearLayoutManager(context));
		}
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
		View v = LayoutInflater.from(viewGroup.getContext())
				.inflate(R.layout.information_card, viewGroup, false);
		return new Services_ViewHolder(v);
	}

	@Override
	public void onBindViewHolder(ViewHolder viewHolder, final int position) {
		Services_ViewHolder holder = (Services_ViewHolder) viewHolder;
		Service_Details_Model details = mServiceDetails.get(position);
		holder.mServiceName.setText(details.getServiceName());
		holder.mLargeDescription.setText(details.getLargeDescription());
		holder.mAddress.setText(details.getAddress());
		holder.mCategory.setText(details.getCategory());
		holder.mRate.setText(String.format(Locale.getDefault(), "%.2f", details.getRate()));
		holder.mDistance.setText(String.format(Locale.getDefault(), "%.2f", details.getDistance()));
		holder.mBusinessHours.setText(details.getBusinessHours());

		ArrayList<Credit_Card_List_Model> cardsList = mServiceDetails.get(position).getCards();
		holder.mCreditCardsAdapter.setData(cardsList);
	}

	@Override
	public int getItemCount() {
		return mServiceDetails.size();
	}

}
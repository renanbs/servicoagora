package com.vikings.servicoagora;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

public class DetailsActivity extends AppCompatActivity implements
			Tab_Details_Controller_Fragment.onEstablishmentId{

	private int mSelectedEstablishmentId;
	private String mServiceName;

	static final String name = "SERVICE_NAME";
	static final String selectedId = "SELECTED_ID";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_details);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		Intent mIntent = getIntent();
		mSelectedEstablishmentId = mIntent.getIntExtra("position", 0);
		mServiceName = mIntent.getStringExtra("serviceName");

		createTabs();
		// Get a support ActionBar corresponding to this toolbar
		ActionBar ab = getSupportActionBar();
		ab.setElevation(0);
		if (!mServiceName.isEmpty())
			ab.setTitle(mServiceName);

		// Enable the Up button
		ab.setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		// Save the user's current game state
		savedInstanceState.putInt(selectedId, mSelectedEstablishmentId);
		savedInstanceState.putString(name, mServiceName);

		// Always call the superclass so it can save the view hierarchy state
		super.onSaveInstanceState(savedInstanceState);
	}

	public void onRestoreInstanceState(Bundle savedInstanceState) {
		// Always call the superclass so it can restore the view hierarchy
		super.onRestoreInstanceState(savedInstanceState);

		// Restore state members from saved instance
		mSelectedEstablishmentId = savedInstanceState.getInt(selectedId);
		mServiceName = savedInstanceState.getString(name);
	}

	private void createTabs(){
		Tab_Details_Controller_Fragment details = new Tab_Details_Controller_Fragment();
		FragmentTransaction fragment = getSupportFragmentManager().beginTransaction();
		fragment.replace(R.id.details_frame, details);
		fragment.commit();
	}
	@Override
	public int getPosition(){
		return mSelectedEstablishmentId;
	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
	}
}

package com.vikings.servicoagora;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class Establishment_RecyclerAdapter extends
		RecyclerView.Adapter<Establishment_RecyclerAdapter.Establishment_ViewHolder>
{

	private ArrayList<Establishment_List_Model> mEstablishment;
	// Define listener member variable
	private static OnItemClickListener listener;

	public Establishment_RecyclerAdapter(ArrayList<Establishment_List_Model> ecs){
		mEstablishment = ecs;
	}

	// Define the listener interface
	public interface OnItemClickListener {
		void onItemClick(View itemView, int position);
	}
	// Define the method that allows the parent activity or fragment to define the listener
	public void setOnItemClickListener(OnItemClickListener listener) {
		this.listener = listener;
	}

	@Override
	public Establishment_ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		Context context = parent.getContext();
		LayoutInflater inflater = LayoutInflater.from(context);

		View v = inflater.inflate(R.layout.establishment_cardview, parent, false);

		Establishment_ViewHolder viewHolder = new Establishment_ViewHolder(v);
		return viewHolder;
	}

	@Override
	public void onBindViewHolder(Establishment_ViewHolder holder, int position) {
		Establishment_List_Model ec = mEstablishment.get(position);
		holder.ecName.setText(ec.getName());
		holder.ecType.setText(ec.getServiceType());
		holder.smallDescr.setText(ec.getSmallDescription());
		String rate = String.format(Locale.getDefault(), "%.1f", ec.getRate());
		holder.rate.setText(rate);
		holder.ratingBar.setRating((float)ec.getRate());

		String dist = String.format(Locale.getDefault(), "%.2f", ec.getDistance()) + "Km";
		holder.distance.setText(dist);
		Log.e("Logo Url", ec.getEstablishmentLogoUrl());

		Context context = holder.itemView.getContext();
		Picasso.with(context).load(ec.getEstablishmentLogoUrl()).fit().into(holder.ecLogo);
	}


	@Override
	public int getItemCount() {
		return mEstablishment.size();
	}

	public class Establishment_ViewHolder extends RecyclerView.ViewHolder {

		TextView ecName, ecType, smallDescr, rate, distance;
		CircleImageView ecLogo;
		RatingBar ratingBar;

		public Establishment_ViewHolder(final View itemView) {
			super(itemView);

			ecName = (TextView) itemView.findViewById(R.id.establishment_title);
			smallDescr = (TextView) itemView.findViewById(R.id.establishment_description);
			ecType = (TextView) itemView.findViewById(R.id.establishment_type);
			ratingBar = (RatingBar) itemView.findViewById(R.id.establishment_ratingBar);
			rate = (TextView) itemView.findViewById(R.id.establishment_rating);
			ecLogo = (CircleImageView) itemView.findViewById(R.id.establishment_logo);
			distance = (TextView) itemView.findViewById(R.id.distance);
			// Setup the click listener
			itemView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// Triggers click upwards to the adapter on click
					if (listener != null)
						listener.onItemClick(itemView, getLayoutPosition());
				}
			});
		}
	}


	/** Filter Logic**/
	public void animateTo(ArrayList<Establishment_List_Model> models) {
		applyAndAnimateRemovals(models);
		applyAndAnimateAdditions(models);
		applyAndAnimateMovedItems(models);
	}

	private void applyAndAnimateRemovals(List<Establishment_List_Model> newModels) {

		for (int i = mEstablishment.size() - 1; i >= 0; i--) {
			final Establishment_List_Model model = mEstablishment.get(i);
			if (!newModels.contains(model)) {
				removeItem(i);
			}
		}
	}

	private void applyAndAnimateAdditions(List<Establishment_List_Model> newModels) {

		for (int i = 0, count = newModels.size(); i < count; i++) {
			final Establishment_List_Model model = newModels.get(i);
			if (!mEstablishment.contains(model)) {
				addItem(i, model);
			}
		}
	}

	private void applyAndAnimateMovedItems(List<Establishment_List_Model> newModels) {

		for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
			final Establishment_List_Model model = newModels.get(toPosition);
			final int fromPosition = mEstablishment.indexOf(model);
			if (fromPosition >= 0 && fromPosition != toPosition) {
				moveItem(fromPosition, toPosition);
			}
		}
	}

	public Establishment_List_Model removeItem(int position) {
		final Establishment_List_Model model = mEstablishment.remove(position);
		notifyItemRemoved(position);
		return model;
	}

	public void addItem(int position, Establishment_List_Model model) {
		mEstablishment.add(position, model);
		notifyItemInserted(position);
	}

	public void moveItem(int fromPosition, int toPosition) {
		final Establishment_List_Model model = mEstablishment.remove(fromPosition);
		mEstablishment.add(toPosition, model);
		notifyItemMoved(fromPosition, toPosition);
	}
}

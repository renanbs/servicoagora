package com.vikings.servicoagora;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class Tab_Details_Pager_Adapter extends FragmentStatePagerAdapter {
	int mNumOfTabs;
	int mId;


	public Tab_Details_Pager_Adapter(FragmentManager fm, int NumOfTabs, int id) {
		super(fm);
		this.mNumOfTabs = NumOfTabs;
		this.mId = id;
	}

	@Override
	public Fragment getItem(int position) {
		Bundle args = new Bundle();
		args.putInt("position", mId);

		switch (position) {
			case 0:
				Tab_Details_Catalog_Fragment tab1 = new Tab_Details_Catalog_Fragment();
				tab1.setArguments(args);
				return tab1;
			case 1:
				Tab_Details_Review_Fragment tab2 = new Tab_Details_Review_Fragment();
				tab2.setArguments(args);
				return tab2;
			case 2:
				Tab_Details_Information_Fragment tab3 = new Tab_Details_Information_Fragment();
				tab3.setArguments(args);
				return tab3;
			default:
				return null;
		}
	}

	@Override
	public int getCount() {
		return mNumOfTabs;
	}
}
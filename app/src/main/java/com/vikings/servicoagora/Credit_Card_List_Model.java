package com.vikings.servicoagora;

/**
 * Created by renan on 5/4/16.
 */
public class Credit_Card_List_Model {

	private int mId;
	private String mBrandName;

	// Same list used in the adapter
	public static final int CASH = 0;
	public static final int VISA = 1;
	public static final int MASTER = 2;
	public static final int PAYPAL = 3;
	public static final int PAGSEGURO = 4;


	public Credit_Card_List_Model(int id, String name){
		mId = id;
		mBrandName = name;
	}

	public String getBrandName() {
		return mBrandName;
	}

	public int getCardId(){
		return mId;
	}

}

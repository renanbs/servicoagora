package com.vikings.servicoagora;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * Created by renan on 05/13/16.
 */
public class Service_List_Workers_Adapter extends
		RecyclerView.Adapter<Service_List_Workers_Adapter.ViewHolder>{

	private ArrayList<String> mDataList;

	public Service_List_Workers_Adapter() {
	}

	public void setData(ArrayList<String> data) {
		if (mDataList != data) {
			mDataList = data;
			notifyDataSetChanged();
		}
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {
		public ViewHolder(View v) {
			super(v);
		}
	}

	public class Services_ViewHolder extends ViewHolder {
		TextView mWorkerName;

		public Services_ViewHolder(View v) {
			super(v);
			this.mWorkerName = (TextView) v.findViewById(R.id.worker);
		}
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
		View v = LayoutInflater.from(viewGroup.getContext())
				.inflate(R.layout.worker_item, viewGroup, false);
		return new Services_ViewHolder(v);
	}

	@Override
	public void onBindViewHolder(ViewHolder viewHolder, final int position) {
		Services_ViewHolder holder = (Services_ViewHolder) viewHolder;
		String w = mDataList.get(position);
		holder.mWorkerName.setText(w);
	}

	@Override
	public int getItemCount() {
		return mDataList.size();
	}

}
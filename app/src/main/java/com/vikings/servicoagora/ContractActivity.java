package com.vikings.servicoagora;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

public class ContractActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contract);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		// Get a support ActionBar corresponding to this toolbar
		ActionBar ab = getSupportActionBar();
		ab.setElevation(0);

		// Enable the Up button
		ab.setDisplayHomeAsUpEnabled(true);

		Contract_Fragment contract = new Contract_Fragment();
		android.support.v4.app.FragmentTransaction contentTransaction = getSupportFragmentManager().beginTransaction();
		contentTransaction.replace(R.id.contract_frame, contract);
		contentTransaction.commit();
	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
	}

	@Override
	public boolean onSupportNavigateUp () {
		onBackPressed();
		return true;
	}
}

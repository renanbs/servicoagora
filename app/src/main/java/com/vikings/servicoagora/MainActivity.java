package com.vikings.servicoagora;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
																Tab_Main_Establishment_List_Fragment.onEstablishmentTouch,
																Tab_Details_Controller_Fragment.onEstablishmentId

{
	private static final String SELECTED_ITEM_ID = "selected";

	private Toolbar mToolbar;
	private NavigationView mNavigationView;
	private DrawerLayout mDrawerLayout;
	private ActionBarDrawerToggle mActionBarDrawerToggle;
	private View mHeaderView = null;

	private int mSelectedId;
	private int mSelectedEstablishmentId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mToolbar = (Toolbar) findViewById(R.id.main_toolbar);
		setSupportActionBar(mToolbar);

		mNavigationView = (NavigationView) findViewById(R.id.main_navigationview);
		mNavigationView.setNavigationItemSelectedListener(this);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

		mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.drawer_open, R.string.drawer_close);
		mDrawerLayout.addDrawerListener(mActionBarDrawerToggle);

		// ADD the header by hand
		mHeaderView = getLayoutInflater().inflate(R.layout.navigation_drawer_header, mNavigationView, false);
		mNavigationView.addHeaderView(mHeaderView);

		// Make the rounded button click work
		ImageView headerImage = (ImageView) mHeaderView.findViewById(R.id.profile_image);
		headerImage.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				login(view);
			}
		});

		mActionBarDrawerToggle.syncState();

//		mSelectedId = savedInstanceState == null ? R.id.sent_mail : savedInstanceState.getInt(SELECTED_ITEM_ID);
		mSelectedId = R.id.menu_start;
		navigate(mSelectedId);

	}


//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main_menu, menu);
//
//		return true;
//	}
//
//	@Override
//	public boolean onPrepareOptionsMenu(Menu menu) {
//		super.onPrepareOptionsMenu(menu);
//		return true;
//	}

	private void navigate(int selectedId)
	{
		switch (selectedId)
		{
			case R.id.menu_start:
				mDrawerLayout.closeDrawer(GravityCompat.START);
				Tab_Main_Controller_Fragment tab_main_controller = new Tab_Main_Controller_Fragment();
				android.support.v4.app.FragmentTransaction contentTransaction = getSupportFragmentManager().beginTransaction();
				contentTransaction.replace(R.id.main_frame, tab_main_controller);
				contentTransaction.commit();
				return;
			case R.id.menu_settings:
				mDrawerLayout.closeDrawer(GravityCompat.START);
				Intent intent = new Intent(this, SettingsActivity.class);
				startActivity(intent);
				return;

			default:
				Toast.makeText(MainActivity.this, "default", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_settings:
				// User chose the "Settings" item, show the app settings UI...
				Intent intent = new Intent(this, SettingsActivity.class);
				startActivity(intent);
				return true;

			default:
				// If we got here, the user's action was not recognized.
				// Invoke the superclass to handle it.
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
//		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onNavigationItemSelected(MenuItem menuItem)
	{
		menuItem.setChecked(true);

		mSelectedId = menuItem.getItemId();
		navigate(mSelectedId);
		return true;
	}

	@Override
	protected void onSaveInstanceState(Bundle bundle)
	{
		super.onSaveInstanceState(bundle);
		bundle.putInt(SELECTED_ITEM_ID, mSelectedId);
	}

	@Override
	public void onBackPressed() {
		if (mDrawerLayout.isDrawerOpen(GravityCompat.START))
		{
			mDrawerLayout.closeDrawer(GravityCompat.START);
		}
		else
		{
			super.onBackPressed();
		}
	}

	public void login(View view) {
		Toast.makeText(MainActivity.this, "Image touched", Toast.LENGTH_SHORT).show();
//		if (!isLogged) {
//			mDrawerLayout.closeDrawer(GravityCompat.START);
//			login_fragment = new LoginFragment();
//			android.support.v4.app.FragmentTransaction loginFragment = getSupportFragmentManager().beginTransaction();
//			loginFragment.replace(R.id.frame, login_fragment);
//			loginFragment.addToBackStack(null);
//			loginFragment.commit();
//		}
	}

	@Override
	public void setPosition(int position){
		mSelectedEstablishmentId = position;
	}

	@Override
	public int getPosition(){
		return mSelectedEstablishmentId;
	}
}

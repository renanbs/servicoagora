package com.vikings.servicoagora;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;


public class Credit_Card_List_Adapter extends
		RecyclerView.Adapter<Credit_Card_List_Adapter.Cards_ViewHolder>{

	private ArrayList<Credit_Card_List_Model> mCards;
	public static final int CASH = 0;
	public static final int VISA = 1;
	public static final int MASTER = 2;
	public static final int PAYPAL = 3;
	public static final int PAGSEGURO = 4;

	public Credit_Card_List_Adapter(){
	}
	public void setData(ArrayList<Credit_Card_List_Model> cards) {
		if (mCards != cards) {
			mCards = cards;
			notifyDataSetChanged();
		}
	}

	@Override
	public Cards_ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		Context context = parent.getContext();
		LayoutInflater inflater = LayoutInflater.from(context);

		View v = inflater.inflate(R.layout.credit_card_item, parent, false);

		Cards_ViewHolder viewHolder = new Cards_ViewHolder(v);
		return viewHolder;
	}

	@Override
	public void onBindViewHolder(Cards_ViewHolder holder, int position) {
		Credit_Card_List_Model ec = mCards.get(position);

		holder.cardName.setText(ec.getBrandName());
		int cardId = ec.getCardId();
		switch (cardId)
		{
			case CASH:
				holder.creditCardLogo.setImageResource(R.drawable.logo_money);
				break;
			case VISA:
				holder.creditCardLogo.setImageResource(R.drawable.logo_visa);
				break;
			case MASTER:
				holder.creditCardLogo.setImageResource(R.drawable.logo_mastercard);
				break;
			case PAYPAL:
				holder.creditCardLogo.setImageResource(R.drawable.logo_paypal);
				break;
			case PAGSEGURO:
				holder.creditCardLogo.setImageResource(R.drawable.logo_pagseguro);
				break;

			default:
				holder.creditCardLogo.setImageResource(R.drawable.cash);
		}
	}


	@Override
	public int getItemCount() {
		return mCards.size();
	}

	public class Cards_ViewHolder extends RecyclerView.ViewHolder {

		TextView cardName;
		ImageView creditCardLogo;

		public Cards_ViewHolder(final View itemView) {
			super(itemView);

			cardName = (TextView) itemView.findViewById(R.id.credit_card_name);
			creditCardLogo = (ImageView) itemView.findViewById(R.id.credit_card_brand_image);
		}
	}
}

package com.vikings.servicoagora;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Tab_Main_Controller_Fragment extends Fragment {


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v =  inflater.inflate(R.layout.tab_main_controller_fragment,container,false);

		TabLayout tabLayout = (TabLayout) v.findViewById(R.id.tab_main_layout);
		tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_main_establishment));
		tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_main_promotions));
		tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_main_bookmarks));
		tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

		final ViewPager viewPager = (ViewPager) v.findViewById(R.id.tab_main_pager);

		final Tab_Main_Pager_Adapter adapter = new Tab_Main_Pager_Adapter(getFragmentManager(), tabLayout.getTabCount());
		viewPager.setAdapter(adapter);
		viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
		tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
			@Override
			public void onTabSelected(TabLayout.Tab tab) {
				viewPager.setCurrentItem(tab.getPosition());
			}

			@Override
			public void onTabUnselected(TabLayout.Tab tab) {

			}

			@Override
			public void onTabReselected(TabLayout.Tab tab) {

			}
		});
		return v;
	}
	public Tab_Main_Controller_Fragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}
}

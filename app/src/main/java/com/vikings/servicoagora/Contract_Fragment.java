package com.vikings.servicoagora;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


public class Contract_Fragment extends Fragment implements View.OnClickListener {
	private EditText fromDateEtxt;
	private EditText toDateEtxt;

	private DatePickerDialog fromDatePickerDialog;
	private DatePickerDialog toDatePickerDialog;
	private TimePickerDialog timePickerDialog;

	private SimpleDateFormat dateFormatter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.contract_fragment, container, false);
		dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
		findViewsById(v);

		setDateTimeField();
		return v;
	}
	private void findViewsById(View v) {
		fromDateEtxt = (EditText) v.findViewById(R.id.etxt_fromdate);
		fromDateEtxt.setInputType(InputType.TYPE_NULL);
		fromDateEtxt.requestFocus();

		toDateEtxt = (EditText) v.findViewById(R.id.etxt_todate);
		toDateEtxt.setInputType(InputType.TYPE_NULL);
	}
	private void setDateTimeField() {
		fromDateEtxt.setOnClickListener(this);
		toDateEtxt.setOnClickListener(this);


		Calendar newCalendar = Calendar.getInstance();
		fromDatePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {

			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				Calendar newDate = Calendar.getInstance();
				newDate.set(year, monthOfYear, dayOfMonth);
				fromDateEtxt.setText(dateFormatter.format(newDate.getTime()));
			}

		},newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

//		toDatePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
//
//			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//				Calendar newDate = Calendar.getInstance();
//				newDate.set(year, monthOfYear, dayOfMonth);
//				toDateEtxt.setText(dateFormatter.format(newDate.getTime()));
//			}
//
//		},newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

		timePickerDialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
			@Override
			public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
				toDateEtxt.setText(new StringBuilder()
						.append(selectedHour).append(":")
						.append(selectedMinute));
			}
		}, newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE), true);//Yes 24 hour time
	}


	@Override
	public void onClick(View v) {
		if(v == fromDateEtxt) {
			fromDatePickerDialog.show();
		} else if(v == toDateEtxt) {
			timePickerDialog.show();
		}
	}
}
package com.vikings.servicoagora;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class Tab_Main_Pager_Adapter extends FragmentStatePagerAdapter {
	int mNumOfTabs;

	public Tab_Main_Pager_Adapter(FragmentManager fm, int NumOfTabs) {
		super(fm);
		this.mNumOfTabs = NumOfTabs;
	}

	@Override
	public Fragment getItem(int position) {

		switch (position) {
			case 0:
				Tab_Main_Establishment_List_Fragment tab1 = new Tab_Main_Establishment_List_Fragment();
				return tab1;
			case 1:
				Tab_Main_Promotions_Fragment tab2 = new Tab_Main_Promotions_Fragment();
				return tab2;
			case 2:
				Tab_Main_Bookmarks_Fragment tab3 = new Tab_Main_Bookmarks_Fragment();
				return tab3;
			default:
				return null;
		}
	}

	@Override
	public int getCount() {
		return mNumOfTabs;
	}
}
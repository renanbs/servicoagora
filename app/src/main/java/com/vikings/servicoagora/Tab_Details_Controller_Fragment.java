package com.vikings.servicoagora;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Tab_Details_Controller_Fragment extends Fragment {

	private int mPosition;

	onEstablishmentId mId;
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		if (context instanceof onEstablishmentId) {
			mId = (onEstablishmentId) context;
		} else {
			throw new ClassCastException(context.toString()
					+ " must implement onEstablishmentId");
		}
	}
	@Override
	public void onDetach() {
		mId = null; // => avoid leaking
		super.onDetach();
	}

	public interface onEstablishmentId {
		int getPosition();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v =  inflater.inflate(R.layout.tab_details_controller_fragment,container,false);

		mPosition = mId.getPosition();
		TabLayout tabLayout = (TabLayout) v.findViewById(R.id.tab_details_layout);
		tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_details_catalog));
//		tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_details_employees));
		tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_details_reviews));
		tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_details_information));
		tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

		final ViewPager viewPager = (ViewPager) v.findViewById(R.id.tab_details_pager);

		final Tab_Details_Pager_Adapter adapter = new Tab_Details_Pager_Adapter(getFragmentManager(), tabLayout.getTabCount(), mPosition);
		viewPager.setAdapter(adapter);
		viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
		tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
			@Override
			public void onTabSelected(TabLayout.Tab tab) {
				viewPager.setCurrentItem(tab.getPosition());
			}

			@Override
			public void onTabUnselected(TabLayout.Tab tab) {

			}

			@Override
			public void onTabReselected(TabLayout.Tab tab) {

			}
		});
		return v;
	}
	public Tab_Details_Controller_Fragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	public void setPosition(int position){
		mPosition = position;
	}
}

package com.vikings.servicoagora;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

public class SettingsActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		SharedPreferences settings = getSharedPreferences(getString(R.string.settings_preferences), MODE_PRIVATE);

		// Reading from SharedPreferences
		String url = settings.getString(getString(R.string.settings_server_url), getString(R.string.settings_server_url_default));
		EditText ed1 = (EditText) findViewById(R.id.editText_serverURL);
		ed1.setText(url);

		String service = settings.getString(getString(R.string.settings_establishment_list), getString(R.string.settings_server_url_default));
		EditText ed2 = (EditText) findViewById(R.id.editText_establishment_list);
		ed2.setText(service);

		String serviceList = settings.getString(getString(R.string.settings_services_list), getString(R.string.settings_server_url_default));
		EditText edServicesList = (EditText) findViewById(R.id.editText_services_list);
		edServicesList.setText(serviceList);

		String ecInfo = settings.getString(getString(R.string.settings_service_ec_info), getString(R.string.settings_server_url_default));
		EditText ecInfoText = (EditText) findViewById(R.id.editText_ec_info);
		ecInfoText.setText(ecInfo);

		FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				// Writing data to SharedPreferences
				SharedPreferences settings = getSharedPreferences(getString(R.string.settings_preferences), MODE_PRIVATE);
				SharedPreferences.Editor editor = settings.edit();
				EditText ed1 = (EditText) findViewById(R.id.editText_serverURL);
				String url = ed1.getText().toString();
				editor.putString(getString(R.string.settings_server_url), url);

				EditText ed2 = (EditText) findViewById(R.id.editText_establishment_list);
				String serv = ed2.getText().toString();
				editor.putString(getString(R.string.settings_establishment_list), serv);

				EditText edServicesList = (EditText) findViewById(R.id.editText_services_list);
				String servList = edServicesList.getText().toString();
				editor.putString(getString(R.string.settings_services_list), servList);

				EditText ecInfoText = (EditText) findViewById(R.id.editText_ec_info);
				String ecInfo = ecInfoText.getText().toString();
				editor.putString(getString(R.string.settings_service_ec_info), ecInfo);

				editor.commit();
				Snackbar.make(view, getString(R.string.settings_saved_message_ok), Snackbar.LENGTH_LONG)
						.setAction("Action", null).show();
			}
		});

		// Get a support ActionBar corresponding to this toolbar
		ActionBar ab = getSupportActionBar();

		// Enable the Up button
		ab.setDisplayHomeAsUpEnabled(true);
	}

}

package com.vikings.servicoagora;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Tab_Main_Establishment_List_Fragment extends Fragment implements SearchView.OnQueryTextListener
{
	SwipeRefreshLayout mSwipeRefreshLayout;
	RecyclerView mRecyclerView;
	private ArrayList<Establishment_List_Model> mEcList;
	private ArrayList<Establishment_List_Model> mEcListOriginal;
	Establishment_RecyclerAdapter mAdapter;
	ProgressDialog mProgressDialog;
	String mServerUrl;
	String mService;

	onEstablishmentTouch mTouch;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.tab_main_establishment_list_fragment, container, false);
		setHasOptionsMenu(true);
		mProgressDialog = new ProgressDialog(getContext());
		mRecyclerView = (RecyclerView) v.findViewById(R.id.establishment_recyclerview);
		mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.establishment_list_swipe_container);
		mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
													 @Override
													 public void onRefresh() {
														 refreshContent();
													 }
												 });
		mSwipeRefreshLayout.setColorSchemeColors(R.color.colorAccent, R.color.colorSecondary);

		SharedPreferences settings = getActivity().getSharedPreferences(getString(R.string.settings_preferences), getActivity().MODE_PRIVATE);
		mServerUrl = settings.getString(getString(R.string.settings_server_url), getString(R.string.settings_server_url_default));
		mService = settings.getString(getString(R.string.settings_establishment_list), getString(R.string.settings_server_url_default));

		if (mEcList == null)
			mEcList = new ArrayList<Establishment_List_Model>();

		getJSON(mServerUrl + mService);

		mAdapter = new Establishment_RecyclerAdapter(mEcList);

		mAdapter.setOnItemClickListener(new Establishment_RecyclerAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(View view, int position) {
				int id = mEcList.get(position).getId();
				mTouch.setPosition(id);
				Intent intent = new Intent(getActivity(), DetailsActivity.class);
				intent.putExtra("position", id);
				String name = mEcList.get(position).getName();
				intent.putExtra("serviceName", name);
				startActivity(intent);
			}
		});

		mRecyclerView.setAdapter(mAdapter);
		mRecyclerView.setHasFixedSize(true);
		mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

		return v;
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		if (context instanceof onEstablishmentTouch) {
			mTouch = (onEstablishmentTouch) context;
		} else {
			throw new ClassCastException(context.toString()
					+ " must implement onEstablishmentTouch");
		}
	}

	private void refreshContent() {
		mEcList.clear();
		getJSON(mServerUrl + mService);
		mSwipeRefreshLayout.setRefreshing(false);
	}

	public void getJSON(String url) {

		// Tag used to cancel the request
		String tag_json_arry = "json_array_req";

		mProgressDialog.setMessage("Loading...");
		mProgressDialog.show();

		final JsonArrayRequest req = new JsonArrayRequest(url,
				new Response.Listener<JSONArray>() {
					@Override
					public void onResponse(JSONArray response) {
						Log.e("Ok = " + response.length() + " : ", response.toString());
						for (int i = 0; i < response.length(); i++)
						{
							try {
								JSONObject c = response.getJSONObject(i);
								int id = c.getInt("id");
								String ec_name = c.getString("name");
								String type = c.getString("type");
								double rate = c.getDouble("rate");
								String logoUrl = mServerUrl + c.getString("image");
								String smallDescr = c.getString("description_small");
								double distance = c.getDouble("distance");
								//Lista de trabalhadores
								JSONArray workersJson = c.getJSONArray("services");
								ArrayList<String> services = new ArrayList<>();
								for (int w = 0; w < workersJson.length(); w++) {
									services.add(workersJson.getString(w));
								}
								Establishment_List_Model ec = new Establishment_List_Model(id, ec_name, type, rate, logoUrl, smallDescr, distance, services);

								mEcList.add(ec);
							}
							catch(JSONException e)
							{
								Log.e("Error: Exception", e.getMessage());
							}
						}
						mAdapter.notifyDataSetChanged();
						mProgressDialog.hide();
					}
				}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				VolleyLog.d("Error", "Error: " + error.getMessage());
				mProgressDialog.hide();
			}
		});

		// Adding request to request queue
		ServerAccess.getInstance(getContext()).addToRequestQueue (req);
	}

	@Override
	public void onStop() {
		super.onStop();
		if(mProgressDialog.isShowing()){
			mProgressDialog.dismiss();
		}
	}

	@Override
	public void onDetach() {
		mTouch = null; // => avoid leaking
		super.onDetach();
	}

	public interface onEstablishmentTouch {
		void setPosition(int position);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear(); //Avoid duplicate items
		inflater.inflate(R.menu.main_menu, menu);

		final MenuItem item = menu.findItem(R.id.action_search);
		final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
		searchView.setOnQueryTextListener(this);

		MenuItemCompat.setOnActionExpandListener(item,
				new MenuItemCompat.OnActionExpandListener() {
					@Override
					public boolean onMenuItemActionCollapse(MenuItem item) {
						// Do something when collapsed
						mEcList  = new ArrayList<>(mEcListOriginal);
						mAdapter.animateTo(mEcList);
						mAdapter.notifyDataSetChanged();
						mRecyclerView.scrollToPosition(0);
						return true; // Return true to collapse action view
					}

					@Override
					public boolean onMenuItemActionExpand(MenuItem item) {
						// Do something when expanded
						mEcListOriginal = new ArrayList<>(mEcList);
						return true; // Return true to expand action view
					}
				});
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		final ArrayList<Establishment_List_Model> filteredModelList = filter(mEcList, newText);
		mAdapter.animateTo(filteredModelList);
		mRecyclerView.scrollToPosition(0);
		return true;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		return false;
	}

	private ArrayList<Establishment_List_Model> filter(ArrayList<Establishment_List_Model> models, String query) {
		query = query.toLowerCase();

		final ArrayList<Establishment_List_Model> filteredModelList = new ArrayList<>();
		for (Establishment_List_Model model : models) {
			final String text = model.getName().toLowerCase();
			if (text.contains(query)) {
				filteredModelList.add(model);
				continue;
			}
			final ArrayList<String> services = model.getServicesList();
			for(String service : services)
			{
				String lowered = service.toLowerCase();
				if (lowered.contains(query)) {
					filteredModelList.add(model);
					break;
				}
			}
		}
		return filteredModelList;
	}
}
package com.vikings.servicoagora;

import java.util.ArrayList;

/**
 * Created by renan on 5/13/16.
 */
public class Service_Details_Model {
	private int mId;
	private String mServiceName;
	private String mLargeDescription;
	private String mAddress;
	private String mCategory;
	private double mRate;
	private double mDistance;
	private String mBusinessHours;
	private ArrayList<Credit_Card_List_Model> mCards;

	public Service_Details_Model(int id, String serviceName,
								 String largeDescription,
								 String address,
								 String category,
								 double rate,
								 double distance,
								 String businessHours,
								 ArrayList<Credit_Card_List_Model> cards){
		mId = id;
		mServiceName = serviceName;
		mLargeDescription = largeDescription;
		mAddress = address;
		mCategory = category;
		mRate = rate;
		mDistance = distance;
		mBusinessHours = businessHours;
		mCards = cards;
	}

	public int getId() {
		return mId;
	}
	public String getServiceName() {
		return mServiceName;
	}
	public String getLargeDescription() {
		return mLargeDescription;
	}

	public String getAddress() {
		return mAddress;
	}

	public String getCategory() {
		return mCategory;
	}

	public double getRate() {
		return mRate;
	}

	public double getDistance() {
		return mDistance;
	}

	public String getBusinessHours() {
		return mBusinessHours;
	}

	public ArrayList<Credit_Card_List_Model> getCards() {
		return mCards;
	}
}

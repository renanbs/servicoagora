package com.vikings.servicoagora;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class Tab_Details_Information_Fragment extends Fragment {

	RecyclerView mRecyclerView;
	private ArrayList<Credit_Card_List_Model> mCCList;
	Credit_Card_List_Adapter mAdapter;
	ProgressDialog mProgressDialog;
	String mServerUrl;
	String mService;
	int mId;
	ImageView mLogo;
	String mLogoUrl;
	TextView mEcName, mLargeDescription, mAddress, mCategory, mOpenTime, mDistance, mRating;
	RatingBar mRatingBar;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Get back arguments
		mId = getArguments().getInt("position", 0);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.tab_details_information_fragment, container, false);
		mProgressDialog = new ProgressDialog(getContext());
		mRecyclerView = (RecyclerView) v.findViewById(R.id.info_credit_card_list_recyclerview);

		mLogo = (ImageView) v.findViewById(R.id.info_logo);
		mEcName = (TextView) v.findViewById(R.id.info_name);
		mLargeDescription = (TextView) v.findViewById(R.id.info_large_description);
		mAddress = (TextView) v.findViewById(R.id.info_address);
		mCategory = (TextView) v.findViewById(R.id.info_category);
		mOpenTime = (TextView) v.findViewById(R.id.info_open_time);
		mDistance = (TextView) v.findViewById(R.id.info_distance);
		mRating = (TextView) v.findViewById(R.id.info_rating);
		mRatingBar = (RatingBar) v.findViewById(R.id.info_ratingBar);

		SharedPreferences settings = getActivity().getSharedPreferences(getString(R.string.settings_preferences), getActivity().MODE_PRIVATE);
		mServerUrl = settings.getString(getString(R.string.settings_server_url), getString(R.string.settings_server_url_default));
		mService = settings.getString(getString(R.string.settings_service_ec_info), getString(R.string.settings_server_url_default));

		if (mCCList == null)
			mCCList = new ArrayList<>();

		getInfo();


		mAdapter = new Credit_Card_List_Adapter();
		mAdapter.setData(mCCList);
		mRecyclerView.setAdapter(mAdapter);
		LinearLayoutManager layoutManager = new LinearLayoutManager(getContext()) {
			@Override
			public boolean canScrollVertically() {
				return false;
			}
		};
		mRecyclerView.setLayoutManager(layoutManager);

		return v;
	}

	public void getInfo() {

		// Tag used to cancel the request
		String tag_json_arry = "json_array_req";
		String url = mServerUrl + mService + "?id=" + mId;

		mProgressDialog.setMessage("Loading...");
		mProgressDialog.show();

		final JsonArrayRequest req = new JsonArrayRequest(url,
				new Response.Listener<JSONArray>() {
					@Override
					public void onResponse(JSONArray response) {
						Log.e("Ok = " + response.length() + " : ", response.toString());
						for (int i = 0; i < response.length(); i++)
						{
							try {
								JSONObject c = response.getJSONObject(i);
								int id = c.getInt("id");
								String ecName = c.getString("name");
								mEcName.setText(ecName);
								mLogoUrl = mServerUrl + c.getString("image");
								String category = c.getString("category");
								mCategory.setText(category);
								String largeDescription = c.getString("large_description");
								mLargeDescription.setText(largeDescription);
								String address = c.getString("address");
								mAddress.setText(address);
								double rate = c.getDouble("rate");
								mRating.setText(String.format(Locale.getDefault(), "%.1f", rate));
								mRatingBar.setRating((float)rate);
								double distance = c.getDouble("distance");
								String dist = String.format(Locale.getDefault(), "%.2f", distance) + "Km";
								mDistance.setText(dist);
								String openTime = c.getString("open_time");
								mOpenTime.setText(openTime);
								JSONObject pt = c.getJSONObject("payment_type");
								JSONArray pts = pt.getJSONArray("type");
								for (int z = 0; z < pts.length(); z++) {
									JSONObject ct = pts.getJSONObject(z);
									int cId = ct.getInt("id");
									String cName = ct.getString("name");
									Credit_Card_List_Model cc = new Credit_Card_List_Model(cId, cName);
									mCCList.add(cc);
								}
							}
							catch(JSONException e)
							{
								Log.e("Error: Exception", e.getMessage());
							}
						}
						mAdapter.notifyDataSetChanged();
						Context context = getContext();
						Picasso.with(context).load(mLogoUrl).fit().into(mLogo);
						mProgressDialog.hide();
					}
				}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				VolleyLog.d("Error", "Error: " + error.getMessage());
				mProgressDialog.hide();
			}
		});

		// Adding request to request queue
		ServerAccess.getInstance(getContext()).addToRequestQueue (req);
	}

	@Override
	public void onStop() {
		super.onStop();
		if(mProgressDialog.isShowing()){
			mProgressDialog.dismiss();
		}
	}
}
package com.vikings.servicoagora;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;


/**
 * Created by renan on 05/13/16.
 */
public class Service_List_Adapter extends
		RecyclerView.Adapter<Service_List_Adapter.ViewHolder>{

	private ArrayList<Service_List_Model> mServiceList;
	// Define listener member variable
	private static OnServiceItemClickListener listener;

	private static RecyclerView mWorkersRecycler;

	public Service_List_Adapter(ArrayList<Service_List_Model> serviceList) {
		mServiceList = serviceList;
	}

	// Define the listener interface
	public interface OnServiceItemClickListener {
		void onItemClick(View itemView, int position);
	}

	// Define the method that allows the parent activity or fragment to define the listener
	public void setOnItemClickListener(OnServiceItemClickListener listener) {
		this.listener = listener;
	}

	public static class ViewHolder extends RecyclerView.ViewHolder {
		public ViewHolder(View v) {
			super(v);
		}
	}

	public class Services_ViewHolder extends ViewHolder {
		TextView mServiceName, mPriceMin, mPriceMax, mWorkersTitle;
		private Service_List_Workers_Adapter mWorkersAdapter;
		public Services_ViewHolder(View v) {
			super(v);
			Context context = itemView.getContext();
			this.mServiceName = (TextView) v.findViewById(R.id.service_name);
			this.mPriceMin = (TextView) v.findViewById(R.id.price_min);
			this.mPriceMax = (TextView) v.findViewById(R.id.price_max);
			this.mWorkersTitle = (TextView) v.findViewById(R.id.workers_title);
			itemView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// Triggers click upwards to the adapter on click
					if (listener != null)
						listener.onItemClick(itemView, getLayoutPosition());
				}
			});

			mWorkersRecycler = (RecyclerView) v.findViewById(R.id.workers_list);
			mWorkersAdapter = new Service_List_Workers_Adapter();
			mWorkersRecycler.setAdapter(mWorkersAdapter);
			mWorkersRecycler.setLayoutManager(new LinearLayoutManager(context));
		}
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
		View v = LayoutInflater.from(viewGroup.getContext())
				.inflate(R.layout.service_details_card, viewGroup, false);
		return new Services_ViewHolder(v);
	}

	@Override
	public void onBindViewHolder(ViewHolder viewHolder, final int position) {
		Services_ViewHolder holder = (Services_ViewHolder) viewHolder;
		Service_List_Model cat = mServiceList.get(position);
		holder.mServiceName.setText(cat.getServiceName());
		holder.mPriceMin.setText(String.format(Locale.getDefault(), "%.2f", cat.getPriceMin()));
		holder.mPriceMax.setText(String.format(Locale.getDefault(), "%.2f", cat.getPriceMax()));
		ArrayList<String> workersList = mServiceList.get(position).getWorkersList();
		holder.mWorkersAdapter.setData(workersList);
		if(!workersList.isEmpty())
		{
			holder.mWorkersTitle.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public int getItemCount() {
		return mServiceList.size();
	}

}
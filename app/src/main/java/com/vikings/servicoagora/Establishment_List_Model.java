package com.vikings.servicoagora;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by renan on 4/18/16.
 */
public class Establishment_List_Model {
	private int mId;
	private String mName;
	private String mServiceType;
	private double mRate; // Rating
	private CircleImageView mEstablishmentLogo;
	private String mEstablishmentLogoUrl;
	private double mDistance;	// Distance calculated from the device location or address
	private String mSmallDescription;
	private ArrayList<String> mServicesList;

	public Establishment_List_Model(int id, String name, String service_type, double rate, String logoUrl, String smallDescription, double distance, ArrayList<String> services){
		mId = id;
		mName = name;
		mServiceType = service_type;
		mDistance = distance;
		mRate = rate;
		mEstablishmentLogoUrl = logoUrl;
		mSmallDescription = smallDescription;
		mServicesList = services;
	}

	public int getId() {
		return mId;
	}
	public String getName() {
		return mName;
	}
	public String getServiceType() {
		return mServiceType;
	}
	public double getRate() {
		return mRate;
	}
	public CircleImageView getEstablishmentLogo() {
		return mEstablishmentLogo;
	}
	public double getDistance() { return mDistance; }
	public String getSmallDescription() { return mSmallDescription; }

	public void setEstablishmentLogo(CircleImageView mEstablishmentLogo) {
		this.mEstablishmentLogo = mEstablishmentLogo;
	}

	public String getEstablishmentLogoUrl() {
		return mEstablishmentLogoUrl;
	}

	public ArrayList<String> getServicesList() {
		return mServicesList;
	}
}
